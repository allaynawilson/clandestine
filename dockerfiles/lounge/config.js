"use strict";
module.exports = {
        public: true,
        host: "100.64.79.34",
        port: 80,
        bind: "100.64.69.227",
        reverseProxy: true,
        maxHistory: 0,
        https: {
                enable: false,
                key: "",
                certificate: "",
                ca: "",
        },
        theme: "default",
        prefetch: true,
        disableMediaPreview: false,
        prefetchStorage: true,
        prefetchMaxImageSize: 8192,
        fileUpload: {
                enable: false,
                maxFileSize: 10240,
                baseUrl: null,
        },
        transports: ["websocket"],
        leaveMessage: "gone",
        defaults: {
                name: "𝓬𝓵𝓪𝓷𝓭𝓮𝓼𝓽𝓲𝓷𝓮",
                host: "100.64.69.226",
                port: 6697,
                password: "",
                tls: true,
                rejectUnauthorized: false,
                nick: "guest-%%",
                username: "webirc",
                realname: "web IRC",
                join: "#dmz",
        },
        lockNetwork: true,
        messageStorage: ["sqlite", "text"],
        useHexIp: false,
        webirc: {
            "100.64.69.226": (webircObj, network) => {
              webircObj.password = "changeme";
              webircObj.hostname = `${webircObj.hostname}`.split(' ')[0];
              return webircObj;
           },
        },
        identd: {
                enable:false,
                port: 113,
        },
        oidentd: null,
        ldap: {
                enable: false,
                tlsOptions: {},
                primaryKey: "uid",
                searchDN: {
                        rootDN: "cn=thelounge,ou=system-users,dc=example,dc=com",
                        rootPassword: "1234",
                        filter: "(objectClass=person)(memberOf=ou=accounts,dc=example,dc=com)",
                        base: "dc=example,dc=com",
                        scope: "sub",
                },
        },
        debug: {
                ircFramework: false,
                raw: false,
        },
};
