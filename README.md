# DNS configuration
The addresses `185.193.127.161` and `2a0a:3840:1337:127:0:b9c1:7fa1:1337` should be the target addresses of the new server:
- TYPE: `CAA` NAME: `@` CONTENT: `0 issuewild "letsencrypt.org"` TTL: `24h`
- TYPE: `A` NAME: `*` CONTENT: `185.193.127.161` TTL: `24h`
- TYPE: `AAAA` NAME: `*` CONTENT: `2a0a:3840:1337:127:0:b9c1:7fa1:1337` TTL: `24h`
# System configuration (Ubuntu 20.04)
- `apt install network-manager`
- `/etc/netplan/00-installer-config.yaml`:
```
network:
  renderer: NetworkManager
```
- `netplan apply`
- `nmcli con add type dummy ifname DOCKERD ipv4.method manual ipv4.addresses 198.18.0.1/24`
- `nmcli con up dummy-DOCKERD`
- `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`
- add repository:
```
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
- `sudo apt-get update`
- `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin python3 python3-pip nftables`
- `pip3 install docker-compose`
- `systemctl stop docker`

IPv6 is to be disabled by default on the Docker daemon but docker-compose files can specify to enable it for individual containers.

- `/lib/systemd/system/docker.service`:
```
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target docker.socket firewalld.service containerd.service
Wants=network-online.target
Requires=docker.socket containerd.service

[Service]
Type=notify
# the default is not to use systemd for cgroups because the delegate issues still
# exists and systemd currently does not support the cgroup feature set required
# for containers run by docker
ExecStart=/usr/bin/dockerd -H 198.18.0.1 -H fd:// --ipv6=false --iptables=false --ip-masq=false --bip=100.64.60.1/22 --fixed-cidr=100.64.60.0/22 --default-address-pool base=100.64.64.0/20,size=29 --containerd=/run/containerd/containerd.sock
ExecReload=/bin/kill -s HUP $MAINPID
TimeoutSec=0
RestartSec=2
Restart=always

# Note that StartLimit* options were moved from "Service" to "Unit" in systemd 229.
# Both the old, and new location are accepted by systemd 229 and up, so using the old location
# to make them work for either version of systemd.
StartLimitBurst=3

# Note that StartLimitInterval was renamed to StartLimitIntervalSec in systemd 230.
# Both the old, and new name are accepted by systemd 230 and up, so using the old name to make
# this option work for either version of systemd.
StartLimitInterval=60s

# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity

# Comment TasksMax if your systemd version does not support it.
# Only systemd 226 and above support this option.
TasksMax=infinity

# set delegate yes so that systemd does not reset the cgroups of docker containers
Delegate=yes

# kill only the docker process, not all processes in the cgroup
KillMode=process
OOMScoreAdjust=-500

[Install]
WantedBy=multi-user.target
```
- `systemctl daemon-reload`
- `ip link del docker0`
- `ip link add name docker0 type bridge`
- `ip addr add 100.64.60.1/22 dev docker0`
- `cd /var/lib/docker && rm -rf *`
- `rm /etc/docker/key.json`
- `systemctl start docker`
- `systemctl enable docker`
- `/etc/nftables.conf`
```
table inet DEFAULT_FILTER {
        set ICMP_ALLOWED_NETWORKS6 {
                type ipv6_addr
                flags interval
                elements = { fe80::/10 }
        }

        set INGRESS_WHITE_LIST {
                type ipv4_addr
                flags interval
                elements = { 0.0.0.0-255.255.255.255 }
        }

        set INGRESS_WHITE_LIST6 {
                type ipv6_addr
                flags interval
                elements = { :: }
        }

        set PORTS_TCP_IN_FORWARD {
                type inet_service
                flags interval
                elements = { 80, 443, 6667, 6697, 8448 }
        }

        map PORTS_TCP_FORWARD {
                type inet_service : ipv4_addr
                elements = { 80 : 100.64.48.6, 443 : 100.64.48.6, 6667 : 100.64.48.6, 6697 : 100.64.48.6, 8448 : 100.64.48.6 }
        }

        chain NAT_PREROUTING {
                type nat hook prerouting priority filter; policy accept;
                ip saddr 100.64.0.0/10 return
                ip protocol tcp dnat ip to tcp dport map @PORTS_TCP_FORWARD
        }

        chain NAT_POSTROUTING {
                type nat hook postrouting priority filter; policy accept;
                oifname "eth0" ip saddr { 100.64.32.0/19 } masquerade
        }

        chain DROP_NAT_REDIRECT {
                log prefix "NAT_REDIRECT" group 1
                log group 5
                drop
        }

        chain DOCKER_INPUT {
                ip daddr 100.64.48.0/20 ip saddr 100.64.0.0/10 jump DROP_NAT_REDIRECT
                ip daddr 100.64.48.0/20 ip saddr @INGRESS_WHITE_LIST tcp dport @PORTS_TCP_IN_FORWARD accept
                ip daddr { 100.64.32.0/19 } ct state established accept
                ip daddr != 100.64.0.0/10 ct state established accept
                ip daddr 100.64.0.0/10 log group 1
                log prefix "INPUT" group 1
                log group 4
                drop
        }

        chain UNREACHABLE {
                log prefix "REACHABILITY" group 1
                log group 6
                drop
        }

        chain DOCKER_FORWARD {
                ip saddr 100.64.64.0/20 ip daddr 100.64.64.0/20 accept
                ip saddr @INGRESS_WHITE_LIST ip daddr 100.64.48.0/20 tcp dport @PORTS_TCP_IN_FORWARD accept
                ip6 saddr @INGRESS_WHITE_LIST6 tcp dport @PORTS_TCP_IN_FORWARD accept
                ip saddr { 100.64.16.0/20, 100.64.48.0/20 } ip daddr 100.64.32.0/20 accept
                ip saddr 100.64.32.0/20 ip daddr { 100.64.16.0/20, 100.64.48.0/20 } ct state established accept
                ip saddr != 100.64.0.0/10 ip daddr { 100.64.32.0/19 } ct state established accept
                ip saddr 100.64.0.0/10 ip daddr { 100.64.32.0/19 } jump UNREACHABLE
                ip saddr { 100.64.32.0/19 } ip daddr != 100.64.0.0/10 accept
                log prefix "FORWARD" group 1
                log group 2
                drop
        }

        chain DOCKER_OUTPUT {
                ip saddr { 100.64.32.0/19 } ip daddr != 100.64.0.0/10 accept
                ip daddr 100.64.48.0/20 ip saddr != 100.64.0.0/10 accept
                log prefix "OUTPUT" group 1
                log group 3
                drop
        }

        chain INPUT {
                type filter hook input priority filter; policy accept;
                meta nfproto { ipv4, ipv6 } tcp dport 22 accept
                meta iiftype loopback accept
                ip6 nexthdr ipv6-icmp ip6 saddr @ICMP_ALLOWED_NETWORKS6 accept
                ip saddr 100.64.0.0/10 jump DOCKER_INPUT
                ip daddr 100.64.0.0/10 jump DOCKER_INPUT
                ct state established accept
                log prefix "INPUT" group 7
                log group 8
                drop
        }

        chain FORWARD {
                type filter hook forward priority filter; policy accept;
                meta oiftype loopback accept
                ip saddr 100.64.0.0/10 jump DOCKER_FORWARD
                ip daddr 100.64.0.0/10 jump DOCKER_FORWARD
        }

        chain OUTPUT {
                type filter hook output priority filter; policy accept;
                meta oiftype loopback accept
                ip saddr 100.64.0.0/10 jump DOCKER_OUTPUT
                ip daddr 100.64.0.0/10 jump DOCKER_OUTPUT
        }
}
```
- `systemctl start nftables`
- `systemctl enable nftables`
- `/etc/sysctl.conf`
```
net.core.default_qdisc                             = fq
net.core.rmem_max                                  = 134217728
net.core.wmem_max                                  = 134217728
net.ipv4.conf.all.log_martians                     = 1
net.ipv4.tcp_rmem                                  = 4096 87380 67108864
net.ipv4.tcp_wmem                                  = 4096 65536 67108864
net.ipv4.tcp_congestion_control                    = htcp
net.ipv4.tcp_mtu_probing                           = 0
net.ipv4.tcp_timestamps                            = 1
net.ipv4.conf.default.accept_redirects             = 0
net.ipv4.conf.default.secure_redirects             = 1
net.ipv4.conf.default.send_redirects               = 0
net.ipv4.conf.all.rp_filter                        = 2
net.ipv4.conf.all.accept_source_route              = 0
net.ipv4.tcp_syncookies                            = 1
net.ipv6.conf.default.autoconf                     = 0
net.ipv6.conf.default.accept_ra                    = 0
net.ipv6.conf.default.accept_dad                   = 0
net.ipv6.conf.default.accept_redirects             = 0
net.netfilter.nf_conntrack_helper                  = 1
net.netfilter.nf_conntrack_checksum                = 1
net.netfilter.nf_conntrack_tcp_timeout_established = 120
net.netfilter.nf_conntrack_log_invalid             = 255
net.netfilter.nf_conntrack_tcp_timeout_close_wait  = 60
net.netfilter.nf_conntrack_tcp_timeout_fin_wait    = 60
net.netfilter.nf_conntrack_tcp_timeout_time_wait   = 60
net.netfilter.nf_conntrack_max                     = 524288
net.netfilter.nf_conntrack_timestamp               = 1
net.netfilter.nf_conntrack_acct                    = 1
net.ipv4.conf.all.forwarding                       = 1
net.ipv6.conf.all.forwarding                       = 1
```
- `sysctl -f /etc/sysctl.conf`
- `~/.bashrc`:
```
export PATH=/usr/local/bin:$PATH
```
- `source ~/.bashrc`

# Container configuration

## Init
- Create include configuration files from examples and edit them: 

```
cd ~/docker-inspircd
- `./init`
```
- For generating passwords:
```
dd if=/dev/urandom | head -n 10240 | tr -d '[:cntrl:]' | fold -s -w 1 | grep "[0-9a-zA-Z]" | tr -d '\n' | fold -s -w80 | head -n 3
```

- edit `config/docker/.env` first then run `./create`

## Tor daemon
- `cd ~/docker-compose/in_tor`
- `docker-compose up -d`
- Retrieve the tor DMZ hostname and make note of it for later `docker exec -it in_tor_tor_1 cat /var/lib/tor/leaf_6667/hostname`


### inspIRCd vars configuration
- set unique passwords for every `changeme` if it's a password
- set ids incrementally, they must be unique within the entire network, leave defaults if this is the first server.
```
<define
  name="generalid"
  value="31X">

<define
  name="tordmzid"
  value="51X">

<define
  name="dmzid"
  value="71X">

<define
  name="webircid"
  value="81X">

<define
  name="matrixid"
  value="91X">

```
- Set the TOR DMZ hostname that was retrieved earlier 
- `cd ~/docker-inspircd/in_hub && docker-compose create`
- The leaf servers use self signed certificates for linking the internal hub, start a temporary container for initializing the certificate store:
```
docker run -it -v in_hub_private:/inspircd/private -v ~/docker-inspircd/config/inspircd/hub:/inspircd/config -v ~/docker-inspircd/config/inspircd/include/:/inspircd/config_include -v  ~/docker-inspircd/config/shared/:/inspircd/shared -v ~/docker-inspircd/config/inspircd/vars.inspircd.conf:/inspircd/vars.conf -v ~/docker-inspircd/config/inspircd/links.inspircd.conf:/inspircd/links.conf -v ~/ssl-out:/mnt/ -u root --rm inspircd
```
### Setting up primary CA
- In the container run, `easyrsa build-ca nopass` and create the CA. (Default values are fine.)
### Setting up sub CA
*Note: do this if this site is linking to a server that is already setup. This will also ensure seemless linking between sites without having to rely on a single CA.*
- `easyrsa build-ca nopass subca` (specify a common name that is different from the primary)
- Copy the signing request to `/mnt`: `cp /inspircd/private/reqs/ca.req /mnt`
- Outside of the container on the host, upload the signing request from `~/ssl-out` to the primary CA host's `~/ssl-out` directory
- On the primary CA, start a temporary container:
```
docker run -it -v in_hub_private:/inspircd/private -v ~/docker-inspircd/config/inspircd/hub:/inspircd/config -v ~/docker-inspircd/config/inspircd/include/:/inspircd/config_include -v  ~/docker-inspircd/config/shared/:/inspircd/shared -v ~/docker-inspircd/config/inspircd/vars.inspircd.conf:/inspircd/vars.conf -v ~/docker-inspircd/config/inspircd/links.inspircd.conf:/inspircd/links.conf -v ~/ssl-out:/mnt/ --rm inspircd
```
- Sign the request: `easyrsa import-req /mnt/ca.req ca2.inspircd.internal && easyrsa sign-req ca ca2.inspircd.internal`
- Type `yes` to confirm then hit enter
- Copy the `ca2.inspircd.internal` cert to the `/mnt` directory and then copy them to the sub-CA host's `~/ssl-out` directory from the primary CA host
- In the temporary container on the sub-CA host, copy the `.crt` from `/mnt` to `/inspircd/private/ca.crt`
### All CAs
*Note: create all the certificates since these are used internally*
- Generate DH: `easyrsa gen-dh`
- Generate CRL: `easyrsa gen-crl`
- Create all of the certificates and keys:
```
easyrsa build-server-full am.nl.eu.clandestine.network nopass
easyrsa build-server-full redis.am.nl.eu.clandestine.network nopass
easyrsa build-server-full irc.am.nl.eu.clandestine.network nopass
easyrsa build-server-full irc.dmz.am.nl.eu.clandestine.network nopass
easyrsa build-server-full matrix.am.nl.eu.clandestine.network nopass
easyrsa build-server-full matrix.clandestine.network nopass
easyrsa build-server-full proxy.dmz.clandestine.network nopass
easyrsa build-server-full services.clandestine.network nopass
easyrsa build-server-full tor.dmz.clandestine.network nopass
easyrsa build-server-full webirc.am.nl.eu.clandestine.network nopass
easyrsa build-server-full webirc.clandestine.network nopass
chmod +rx /inspircd/private/issued /inspircd/private/private
chmod +r /inspircd/private/ca.crt
chmod +r /inspircd/private/private/redis.am.nl.eu.clandestine.network.key
chmod +r /inspircd/private/issued/redis.am.nl.eu.clandestine.network.crt
```
- exit the temporary container
- `docker-compose up -d`
- Verify that the container started correctly `docker logs in_hub_hub_1` and go back and correct configuration errors if necessary.

## Redis
*Note: Replication needs to be setup and configured for services failover*
- `cd ~/docker-inspircd/in_redis`
- `docker-compose up -d`

## Services
*Note: This only needs to run on one server at a time. Skip this step if it's already running.*
- Create include configuration files from examples and edit them: 
```
cd ~/docker-inspircd
cp config/anope/examples/vars.conf.example config/anope/vars.conf
```
- `cd ~/docker-inspircd/in_services`
- `docker-compose up -d`
- Verify that the container started correctly `docker logs in_services_services_1` and go back and correct errors if necessary.

## Notes
- `private/private/matrix-bridge.clandestine.network.key`
- `docker run -it --rm -v in_homeserver_signing_key:/key --entrypoint /usr/local/bin/python matrixdotorg/synapse /usr/local/bin/generate_signing_key -o /key/matrix.clandestine.network`
- `docker exec -it in_postgres_postgres_1 createuser synapse_user`
- `docker exec -it in_postgres_postgres_1 createdb --encoding=UTF8 --locale=C --template=template0 --owner=synapse_user synapse`
- `docker exec -it in_postgres_postgres_1 createuser matrix_bridge`
- `docker exec -it in_postgres_postgres_1 createdb --encoding=UTF8 --locale=C --template=template0 --owner=synapse_user matrix_bridge`
- `htpasswd -nb admin changeme`
