/* full configuration file example at
 * https://raw.githubusercontent.com/ircd-hybrid/hopm/master/doc/reference.conf
 */

.include </vars.conf>

options {
  pidfile                   = "/dev/shm/hopm.pid";
  command_queue_size        = 64;
  command_interval          = 10 seconds;
  command_timeout           = 180 seconds;
  negcache_rebuild          = 12 hours;
  dns_fdlimit               = 102400;
  dns_timeout               = 5 seconds;
};

opm {
  blacklist {
    name                    = "dnsbl.dronebl.org";
    address_family          = ipv4, ipv6;
    type                    = "A record reply";
    ban_unknown             = yes;

    reply {
      2                     = "Sample data used for heuristical analysis";
      3                     = "IRC spam drone (litmus/sdbot/fyle)";
      5                     = "Bottler (experimental)";
      6                     = "Unknown worm or spambot";
      7                     = "DDoS drone";
      8                     = "Open SOCKS proxy";
      9                     = "Open HTTP proxy";
      10                    = "ProxyChain";
      11                    = "Web Page Proxy";
      12                    = "Open DNS Resolver";
      13                    = "Automated dictionary attacks";
      14                    = "Open WINGATE proxy";
      15                    = "Compromised router / gateway";
      16                    = "Autorooting worms";
      17                    = "Automatically determined botnet IPs (experimental)";
      18                    = "DNS/MX type hostname detected on IRC";
      255                   = "Uncategorized threat class";
    };
    kline                   = "KLINE 180 *@%i :Try connecting through the proxy DMZ or Tor instead.";
  };

  blacklist {
    name                    = "rbl.efnetrbl.org";
    type                    = "A record reply";
    ban_unknown             = yes;

    reply {
      1                     = "Open proxy";
      2                     = "spamtrap666";
      3                     = "spamtrap50";
      4                     = "TOR";
      5                     = "Drones / Flooding";
    };
    kline                   = "KLINE 180 *@%i :Try connecting through the proxy DMZ or Tor instead.";
  };

  blacklist {
    name                    = "tor.efnetrbl.org";
    type                    = "A record reply";
    ban_unknown             = no;

    reply {
      1                     = "TOR";
    };
    kline                   = "KLINE 180 *@%i :Try connecting through the proxy DMZ or Tor instead.";
  };
};

scanner {
  name                      = "extended";
  protocol                  = HTTP:81;
  protocol                  = HTTP:8000;
  protocol                  = HTTP:8001;
  protocol                  = HTTP:8081;
  protocol                  = HTTPPOST:81;
  protocol                  = HTTPPOST:6588;
  protocol                  = HTTPPOST:4480;
  protocol                  = HTTPPOST:8000;
  protocol                  = HTTPPOST:8001;
  protocol                  = HTTPPOST:8080;
  protocol                  = HTTPPOST:8081;
  protocol                  = SOCKS4:4914;
  protocol                  = SOCKS4:6826;
  protocol                  = SOCKS4:7198;
  protocol                  = SOCKS4:7366;
  protocol                  = SOCKS4:9036;
  protocol                  = SOCKS5:4438;
  protocol                  = SOCKS5:5104;
  protocol                  = SOCKS5:5113;
  protocol                  = SOCKS5:5262;
  protocol                  = SOCKS5:5634;
  protocol                  = SOCKS5:6552;
  protocol                  = SOCKS5:6561;
  protocol                  = SOCKS5:7464;
  protocol                  = SOCKS5:7810;
  protocol                  = SOCKS5:8130;
  protocol                  = SOCKS5:8148;
  protocol                  = SOCKS5:8520;
  protocol                  = SOCKS5:8814;
  protocol                  = SOCKS5:9100;
  protocol                  = SOCKS5:9186;
  protocol                  = SOCKS5:9447;
  protocol                  = SOCKS5:9578;
  protocol                  = SOCKS5:10000;
  protocol                  = SOCKS5:64101;
  protocol                  = SOCKS4:29992;
  protocol                  = SOCKS4:38884;
  protocol                  = SOCKS4:18844;
  protocol                  = SOCKS4:17771;
  protocol                  = SOCKS4:31121;
  fd                        = 102400;
};

scanner {
  name                      = "ssh";
  protocol                  = SSH:22;
  target_string             = "SSH-1.99-OpenSSH_5.1";
  target_string             = "SSH-2.0-dropbear_0.51";
  target_string             = "SSH-2.0-dropbear_0.52";
  target_string             = "SSH-2.0-dropbear_0.53.1";
  target_string             = "SSH-2.0-dropbear_2012.55";
  target_string             = "SSH-2.0-dropbear_2013.62";
  target_string             = "SSH-2.0-dropbear_2014.63";
  target_string             = "SSH-2.0-OpenSSH_4.3";
  target_string             = "SSH-2.0-OpenSSH_5.1";
  target_string             = "SSH-2.0-OpenSSH_5.5p1";
  target_string             = "SSH-2.0-ROSSSH";
  target_string             = "SSH-2.0-SSH_Server";
};

user {
  mask                      = "*!*@";
  scanner                   = "extended";
};

user {
  mask                      = "*!squid@*";
  mask                      = "*!nobody@*";
  mask                      = "*!www-data@*";
  mask                      = "*!cache@*";
  mask                      = "*!CacheFlowS@*";
  mask                      = "*!*@*www*";
  mask                      = "*!*@*proxy*";
  mask                      = "*!*@*cache*";
  scanner                   = "extended";
};
